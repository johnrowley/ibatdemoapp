angular.module('starter.services', [])
.factory('Auth',function ($firebaseAuth, FIREBASE_URI){
        var ref = new Firebase(FIREBASE_URI);
        return $firebaseAuth(ref);
        })
.factory('Chats', function($firebase, FIREBASE_URI, $firebaseAuth, $firebaseArray, $firebaseObject, Auth) {
  // Might use a resource here that returns a JSON array

    var ref = new Firebase(FIREBASE_URI);
    
   
    Auth.$onAuth(function (authData) {
   
    
    if (authData) {
        
        uid = authData.uid;
        chats = $firebaseArray(ref.child("profiles").child(uid).child("chats"))
        
    } else {
        
        chats = [];
        
    }
    
    
    });


  return {
    all: function() {
     var uid = Auth.$getAuth().uid;
     chats = $firebaseArray(ref.child("profiles").child(uid).child("chats"));
    // console.log(chats);
    
    return chats;
     
    },
    add: function (chat) {
        
        chats.$add(chat);
        
    },
    remove: function(chat) {
      //chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
});
