angular.module('starter.controllers', [])

.controller('DateCtrl', function($scope, 
    $firebase, 
    
    FIREBASE_URI, $firebaseArray)
    {
    var ref = new Firebase(FIREBASE_URI);
    
    $scope.listOfDates = [];
    var dates = $firebaseArray(ref.child("reservationObjects"));
   
       
        
        $scope.listOfDates = dates;
    
    $scope.incident  = {};
    $scope.incident.from_dt = $scope.incident.to_dt = new Date();
    $scope.addDate = function () {
        
        //alert($scope.cal.today);
        
        var timestring_from = $scope.incident.from_dt.getTime();
        var timestring_to = $scope.incident.to_dt.getTime();
        
        var incidentObject = {
            from_dt : timestring_from, 
            to_dt : timestring_to,
        symtoms : $scope.incident.symptoms,
        allergies : $scope.incident.allergies,
        medications : $scope.incident.medications
    
        };
        
       // alert(reservationObject);
        
        var incidents = $firebaseArray(ref.child("incidentObjects"));
   
        incidents.$add(incidentObject);
        
        $scope.listOfDates = dates;
       
        
    }
    
    
    $scope.addDate01 = function () {
        
        
       //alert($scope.cal.addDays);
        
       // alert($scope.currentDT.getTime());
        
        $scope.secondsInDay = 60 *60 *24 * 1000 * $scope.cal.addDays;
        $scope.currentTime = $scope.currentDT.getTime();
       
        var currentTime = $scope.currentTime + $scope.secondsInDay;
        $scope.currentDT.setTime($scope.currentTime + $scope.secondsInDay);
        var dates = $firebaseArray(ref.child("timestamps"));
   
        dates.$add(currentTime);
        //alert($scope.currentDT);
   
        $scope.listOfDates = $firebaseArray(ref.child("timestamps"));
        
    }
    
    
})  
.controller('LoginCtrl', function(FIREBASE_URI,  
    $firebaseAuth, $scope, $location) {
    
    var firebaseObject = new Firebase(FIREBASE_URI);
    var loginObject = $firebaseAuth(firebaseObject);
    
    $scope.user = {};
    $scope.user.email ="demo@test.com";
    $scope.user.password = "letmein01";
    
    $scope.login = function () {
        
        var username = $scope.user.email;
        var password  = $scope.user.password
                
        loginObject.$authWithPassword({
            
            email: username,
            password: password
            
            
            
        }).then(function (user){
            
            
            $location.path('/tab/chats');
            
        });
        
        
    }
    
    
    
})

.controller('RegisterCtrl', function($scope,FIREBASE_URI) {
    
    
    $scope.user = {};
    
    $scope.user.email = "demo@test.com";
    $scope.user.password = "letmein01";
    $scope.user.points = 10;
    
    $scope.signup = function () {
        
        alert('sign up ' + $scope.user.email);
        
        var ref = new Firebase(FIREBASE_URI);
        
        ref.createUser({
            
            email: $scope.user.email,
            password: $scope.user.password
            
        }, function (error, authData) {
            if (error) {
             alert('Error: email in use');
             } else {
                 
                 alert('added a user');
                 
                 // Now add a profile that can be updated
                 $scope.profile($scope.user, authData);
                 
             }
            
        });
        
 
    }
    
    
    $scope.profile = function (user, authData, $firebaseAuth, $firebaseArray) {
        
        var ref = new Firebase(FIREBASE_URI);
        
        var users = ref.child("profiles");
        
        users.child(authData.uid).set(user);
      
        
    }
    
    
})

.controller('DashCtrl', function($scope) {})


.controller('ChatsAddCtrl', function($scope, Chats, $location) {

$scope.chat  = {};
$scope.chat.name= "";
$scope.chat.lastText="";

$scope.chatService = Chats;

$scope.addChat = function () {
    
    
    alert('adding chat');
    
    $scope.chatService.add($scope.chat);
    
    $location.path("/tab/chats");
    
};


            
        }).controller('ChatsCtrl', function(FIREBASE_URI, $firebaseObject, $scope, Chats, Auth, $location) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});
  $scope.logout = function () {
      
      Auth.$unauth();
      $location.path('/tabs/login');
      
      
  }
  
  $scope.points = 5;
  
  $scope.addPoints = function () {
        var ref = new Firebase(FIREBASE_URI);
       var uid = Auth.$getAuth().uid;
       $scope.obj = $firebaseObject(ref.child("profiles").child(uid).child("points"));
    
      // console.log(chats);
    
        //alert(pointsObject.val());
     
  }
  
  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  }
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});
